const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighters() {
        const fighters = FighterRepository.getAll();
        if (!fighters) {
            throw new Error("Fighters not found!");
        }
        return fighters;
    }

    getFighterById(id) {
        const fighter = FighterRepository.getOne({ id });
        if (!fighter) {
            throw new Error(`Fighter not found by id = ${id}!`);
        }
        return fighter;
    }

    createFighter(data) {
        const fighter = FighterRepository.create(data);
        if (!fighter) {
            throw new Error("Fighter not created!");
        }
        return fighter;
    }

    updateFighter(id, data) {
        const fighter = FighterRepository.update(id, data);
        if (!fighter) {
            throw new Error(`Fighter not updated by id = ${id}!`);
        }
        return fighter;
    }

    deleteFighter(id) {
        const fighter = FighterRepository.delete(id);
        if (!fighter) {
            throw new Error(`Fighter not deleted by id = ${id}!`);
        }
        return fighter;
    }

    search(search) {
        const fighterSearch = FighterRepository.getOne(search);
        if (!fighterSearch) {
            return null;
        }
        return fighterSearch;
    }
}

module.exports = new FighterService();