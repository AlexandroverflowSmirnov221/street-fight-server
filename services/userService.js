const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            throw new Error("Users not found!");
        }
        return users;
    }

    getUserById(id) {
        const user = UserRepository.getOne({ id });
        if (!user) {
            throw new Error(`User not found by id = ${id}!`);
        }
        return user;
    }

    createUser(data) {
        const user = UserRepository.create(data);
        if (!user) {
            throw new Error("User not created!");
        }
        return user;
    }

    updateUser(id, data) {
        const user = UserRepository.update(id, data);
        if (!user) {
            throw new Error(`User not updated by id = ${id}!`);
        }
        return user;
    }

    deleteUser(id) {
        const user = UserRepository.delete(id);
        if (!user) {
            throw new Error(`User not deleted by id = ${id}!`);
        }
        return user;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

}

module.exports = new UserService();