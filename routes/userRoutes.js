const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
   try {
      const users = UserService.getUsers();
      res.status(200).json(users);
   } catch (err) {
      res.status(404).json('Users not found!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
   try {
      const { id } = req.params;
      const user = UserService.getUserById(id);
      res.status(200).json(user);
   } catch (err) {
      res.status(404).json('User not found!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
   if (res.err) {
      next();
      return;
   }

   try {
      const user = UserService.createUser(req.body);
      res.status(200).json(user);
   } catch (err) {
      res.status(404).json('User not created!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
   if (res.err) {
      next();
      return;
   }

   try {
      const { id } = req.params;
      const user = UserService.updateUser(id, req.body);
      res.status(200).json(user);
   } catch (err) {
      res.status(404).json('User not updated!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
   try {
      const { id } = req.params;
      const user = UserService.deleteUser(id);
      res.status(200).json(user);
   } catch (err) {
      res.status(404).json('User not deleted!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

module.exports = router;