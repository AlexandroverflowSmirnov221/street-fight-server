const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next) => {
   try {
      const fighters = FighterService.getFighters();
      res.status(200).json(fighters);
   } catch (err) {
      res.status(404).json('Users not found!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
   try {
      const { id } = req.params;
      const fighter = FighterService.getFighterById(id);
      res.status(200).json(fighter);
   } catch (err) {
      res.status(404).json('User not found!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
   if (res.err) {
      next();
      return;
    }

   try {
      const fighter = FighterService.createFighter(req.body);
      res.status(200).json(fighter);
   } catch (err) {
      res.status(404).json('User not created!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
   if (res.err) {
      next();
      return;
    }

   try {
      const { id } = req.params;
      const fighter = FighterService.updateFighter(id, req.body);
      res.status(200).json(fighter);
   } catch (err) {
      res.status(404).json('User not updated!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);


router.delete('/:id', (req, res, next) => {
   try {
      const id = req.params.id;
      const fighter = FighterService.deleteFighter(id);
      res.status(200).json(fighter);
   } catch (err) {
      res.status(404).json('User not deleted!');
      res.err = err;
   } finally {
      next();
   }
}, responseMiddleware);

module.exports = router;