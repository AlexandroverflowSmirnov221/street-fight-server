const { user } = require('../models/user');
const EMAIL_REGEXP = new RegExp(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/);
const NUMBER_REGEXP = new RegExp(/^\+?([3][8][0])\)?([0-9]{9})$/);
const UserService = require("../services/userService");

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        const { firstName, lastName, email, phoneNumber, password } = req.body;

        if (req.body.id) {
            throw new Error("Id cannot be here");
        }

        if (!user) {
            throw new Error("Unknown field");
        }

        if (!firstName) {
            throw new Error("User's firstName to create isn't valid");
        }

        if (!lastName) {
            throw new Error("User's lastName to create isn't valid");
        }

        if (!email || !EMAIL_REGEXP.test(email) || !email.includes('@gmail.com')) {
            throw new Error("User's email to create isn't valid");
        }

        if (!phoneNumber || !NUMBER_REGEXP.test(phoneNumber)) {
            throw new Error("User's phoneNumber to create isn't valid");
        }

        if (!password || !password.length >= 3) {
            throw new Error("User's password to create isn't valid")
        }

        if (Object.keys(req.body).length == 0) {
            throw new Error('Data is empty');
        }
        next();
    } catch (e) {
        res.status(400).json(e.message);
    }
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try {
        const { id } = req.params;
        const fields = Object.keys(req.body);
        const userKeys = Object.keys(user);

        if (!fields.every(key => userKeys.indexOf(key) >= 0) || id || !UserService.getUserById(id) || Object.keys(fields).length == 0) {
            throw new Error("User Validation error");
        } else {
            for (let key in req.body) {
                if (key === 'email' && (!req.body[key].includes('@gmail.com') || !EMAIL_REGEXP.test(req.body[key]))) {
                    throw new Error("User's email to update isn't valid")
                    break;
                }
                if (key === 'password' && req.body[key].length < 3) {
                    throw new Error("User's password to update isn't valid")
                    break;
                }
                if (key === 'phoneNumber' && !NUMBER_REGEXP.test(req.body[key])) {
                    throw new Error("User's phoneNumber to update isn't valid")
                    break;
                }
            }
            next();
        }
    } catch (e) {
        res.status(400).json(e.message);
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;