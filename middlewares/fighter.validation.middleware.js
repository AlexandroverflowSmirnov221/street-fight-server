const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try {
        const { name, health = 100, power, defense } = req.body;
        console.log(power)
        console.log(req.body)

        if (req.body.id) {
            throw new Error("Id cannot be here");
        }
        if (!name) {
            throw new Error("Fighter's name to create isn't valid");
        }
        if (!health < 80 || health > 120) {
            throw new Error("Fighter's health to create isn't valid");
        }
        if (power < 1 || power > 100) {
            throw new Error("Fighter's power to create isn't valid");
        }

        if (defense < 1 || defense > 10) {
            throw new Error("Fighter's defense to create isn't valid");
        }

        if (!Number.isInteger(health)) {
            throw new Error("Health must be an integer");
        }

        if (!Number.isInteger(power)) {
            throw new Error("Power must be an integer");
        }

        if (!Number.isInteger(defense)) {
            throw new Error("Defense must be an integer");
        }

        if (Object.keys(req.body).length == 0) {
            throw new Error("Data is empty");
        }

        if (!req.body) {
            throw new Error("Unknown field");
        }

        next();
    } catch (e) {
        res.status(400).json(e.message);
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try {
        const { id } = req.params;
        const fields = Object.keys(req.body);
        const fighterKeys = Object.keys(fighter);

        if (!fields.every(key => fighterKeys.indexOf(key) >= 0) || req.body.id || FighterService.getFighterById(id) || Object.keys(fields).length == 0) {
            throw new Error('Fighter Validation error');
        } else
            for (let key in req.body) {
                if (key === 'health' && (req.body[key] < 80 || req.body[key] > 120 || isNaN(req.body[key]))) {
                    throw new Error("Fighter's health to update isn't valid");
                    break;
                }
                if (key === 'defense' && (req.body[key] < 1 || req.body[key] > 10 || isNaN(req.body[key]))) {
                    throw new Error("Fighter's defense to update isn't valid");
                    break;
                }
                if (key === 'power' && (req.body[key] < 1 || req.body[key] > 100 || isNaN(req.body[key]))) {
                    throw new Error("Fighter's power to update isn't valid");
                    break;
                }
            }
        next();
    } catch (e) {
        res.status(400).json(e.message);
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;