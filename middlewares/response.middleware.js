const responseMiddleware = (req, res, next) => {
    // TODO: Implement middleware that returns result of the query
    try {
        if (res.data) {
            return res.status(200).json(res.data);
        }

        if (req && (req.method === 'GET' || req.method === 'DELETE')) {
            next();
        } else if (req && Object.keys(req.body).length)
            next();
    } catch (e) {
        res.status(400).json({ error: true, message });
    }

}

exports.responseMiddleware = responseMiddleware;